import { Component, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { StudentService } from '../../service/student-service';

export interface PeriodicElement {
  id: number;
  studentId: string;
  name: string;
  surname: string;
  image: string;
  penAmount: number;
  gpa: number;
}

const DATA_STUDENT: PeriodicElement[] = [
  { id: 1, studentId: 'SE-001', name: 'Prayuth', surname: 'The minister', image: 'assets/images/tu.jpg', penAmount: 15, gpa: 3.59 },
  { id: 2, studentId: 'SE-002', name: 'Jurgen', surname: 'Kloop', image: 'assets/images/kloop.jpg', penAmount: 2, gpa: 2.15 },
  { id: 3, studentId: 'SE-003', name: 'Ae+', surname: 'Parena', image: 'assets/images/pareena.jpg', penAmount: 0, gpa: 2.15 },
];

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})

export class StudentsComponent implements OnInit {
  students: Student[];
  displayedColumns: string[] = ['id', 'studentId', 'name', 'surname', 'image', 'penAmount', 'gpa'];
  dataSource = DATA_STUDENT;

  constructor(private studentService: StudentService) { }
  ngOnInit() {

    this.studentService.getStudents()
      .subscribe(students => this.students = students);
  }

  averageGpa() {
    let sum = 0;
    if (Array.isArray(this.students)) {
      for (const student of this.students) {
        sum += student.gpa;
      }
      return sum / this.students.length;
    } else {
      return null;
    }

  }

  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }

}
