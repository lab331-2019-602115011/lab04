import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from 'C:/lab/lab04/src/app/shared/file-not-found/file-not-found.component';
import { StudentsComponent } from './list/students.component';
import { StudentsViewComponent } from './view/students.view.component';
import { StudentsAddComponent } from './add/students.add.component';

const StudentRoutes: Routes = [
    { path: 'add', component: StudentsAddComponent },
    { path: 'list', component: StudentsComponent},
    { path: 'detail/:id', component: StudentsViewComponent},
    { path: '', redirectTo: '/list', pathMatch: 'full'},
    { path: '**', component: FileNotFoundComponent}    
];


@NgModule({
    imports: [
        RouterModule.forRoot(StudentRoutes)
    ],
    exports: [RouterModule]
})

export class StudentRoutingModule { }
